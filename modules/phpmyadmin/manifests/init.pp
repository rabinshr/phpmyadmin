class phpmyadmin($db_user,$db_password,$blowfish){
    $phpmyadmin_archive = 'phpMyAdmin.zip'
    $now = strftime("%s")
    file{
        'phpmyadmin_tmp_dir':
            ensure  =>  directory,
            path    =>  "/tmp/tmp-$now",
            notify  =>  File['phpmyadmin_installer'];
        'phpmyadmin_installer':
              ensure  =>  file,
              path    =>  "/tmp/tmp-$now/${phpmyadmin_archive}",
              notify  =>  Exec['phpmyadmin_extract_installer'],
              source  =>  "puppet:///modules/phpmyadmin/${phpmyadmin_archive}";
    }
    exec {
          'phpmyadmin_extract_installer':
            command      => "unzip -o\
                            /tmp/tmp-$now/${phpmyadmin_archive}\
                            -d /var/www/html/",
            refreshonly  => true,
            require      => [Package['unzip'],File['phpmyadmin_installer']],
            path         => ['/bin','/usr/bin','/usr/sbin','/usr/local/bin'],
            notify       => File['phpmyadmin_config'];

    }

    file {
        'phpmyadmin_config':
            path    => '/var/www/html/phpMyAdmin/config.inc.php',
            ensure  => file,
            content => template('phpmyadmin/config.inc.php.erb'),
            require => Exec['phpmyadmin_extract_installer'],
            notify  => File['phpmyadmin_conf'];
        'phpmyadmin_conf':
            path        => '/etc/httpd/conf.d/phpMyAdmin.conf',
            ensure      => file,
            content     => template('phpmyadmin/phpMyAdmin.conf.erb'),
            require     => File['phpmyadmin_config'],
            notify   => Service['httpd'];

    }

}