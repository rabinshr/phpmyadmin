# == Class: phpmyadmin
#
# Sets up PhpMyAdmin on the server also adds /phpmyadmin alias for easy access
#
# Example Usage:
#
#  class { phpmyadmin:
#    db_user => 'user',
#    db_password => 'password',
#    blowfish => 'blowfish',
#  }
#
# Rabin Shrestha <rabinshr@gmail.com>
#
# Copyright 2013 Rabin Shrestha, unless otherwise noted.
#
class { 'phpmyadmin':
    db_user     => '',
    db_password => '',
    blowfish    => 'your_blowfish_here',
}
